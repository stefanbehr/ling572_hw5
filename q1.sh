#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 5
# Script for running Q1 commands

EX="/dropbox/12-13/572/hw5/examples"

if [ ! -d q1 ]
then
    echo "Creating directory q1..."
    mkdir q1
fi

echo "Training classifier, testing, and saving model..."
vectors2classify --training-file $EX/train2.vectors --testing-file $EX/test2.vectors --trainer MaxEnt --output-classifier q1/m1 >q1/q1.output

echo "Converting classifier to text format..."
classifier2info --classifier q1/m1 >q1/m1.txt

echo "Done."
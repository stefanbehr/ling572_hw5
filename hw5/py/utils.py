# Stefan Behr
# LING 572
# Homework 5

def isolate_feature(feature_value_string):
	"""
	Takes a string in the form of <feature>:<value> and 
	returns only the <feature> substring.
	"""
	return feature_value_string.split(":")[0]

def max_valued_key(d):
	"""
	Takes dict, returns key with the largest value in the dict.
	"""
	max_key = d.keys()[0]
	max_value = d[max_key]
	for key, value in d.items():
		if value > max_value:
			max_value = value
			max_key = key
	return max_key

def init_matrix(rows, cols, init):
	"""
	Creates and returns a len(rows) by len(cols) matrix 
	with each cell initialized to init.
	"""
	return {row: {col: init for col in cols} for row in rows}

def matrix_to_string(matrix, sort=True):
	"""
	Given a matrix in the form of nested dictionaries, 
	returns string representation of matrix for pretty printing.
	"""
	row_labels = matrix.keys()
	col_labels = matrix[row_labels[0]].keys()
	if sort:
		row_labels.sort()
		col_labels.sort()
	cell_width = max(map(len, col_labels))
	row_strings = ["\t".join("{0:{1}}".format(label, cell_width) for label in [""] + col_labels)]
	for row_label in row_labels:
		row = [row_label] + [matrix[row_label][col_label] for col_label in col_labels]
		row_strings.append("\t".join("{0:{1}}".format(cell, cell_width) for cell in row))
	return "\n".join(row_strings)
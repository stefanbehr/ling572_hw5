#!/opt/python-2.7/bin/python2.7

# Stefan Behr
# LING 572
# Homework 5

import sys
from math import exp
from utils import *

if __name__ == "__main__":
	help =	"""\
Three arguments expected:
	sys.argv[1] should be file containing test vectors in plaintext form
	sys.argv[2] should be MaxEnt model file in plaintext form
	sys.argv[3] should be target for storing classification results\
	"""

	if len(sys.argv) < 4:
		print "{0} argument(s) given.".format(len(sys.argv) - 1)
		print help
		exit()
	else:
		test_path = sys.argv[1]
		model_path = sys.argv[2]
		result_path = sys.argv[3]

		class_indicator = "FEATURES FOR CLASS "
		default = "<default>"

		model = {}
		results = []

		# read in and store model
		with open(model_path) as model_file:
			for line in model_file:
				line = line.strip()
				if line.startswith(class_indicator):
					current_class = line[len(class_indicator):]
					model[current_class] = {}
				else:
					if line:
						feature, weight = line.split()
						weight = float(weight)
						model[current_class][feature] = weight

		with open(test_path) as test_file:
			for test_instance in test_file:
				test_instance = test_instance.strip()
				if test_instance:
					test_instance = test_instance.split()
					gold_label = test_instance.pop(0)
					test_instance = [isolate_feature(feat_val_pair) for feat_val_pair in test_instance]

					probabilities = {}
					Z = 0
					for class_label in model:
						summation = model[class_label][default]
						for feature in test_instance:
							summation += model[class_label].get(feature, 0)
						numerator = exp(summation)
						probabilities[class_label] = numerator
						Z += numerator

					probabilities = {class_label: float(numerator) / Z for class_label, numerator in probabilities.items()}
					results.append((gold_label, probabilities))

		classes = model.keys()
		confusion_matrix = init_matrix(classes, classes, 0)	# initialize empty confusion matrix
		correct = 0	# counts correct classifier predictions

		with open(result_path, "w") as result_file:
			for i, result in enumerate(results):
				gold_label, probabilities = result
				# write result line to result file
				print >> result_file, "array:{0}\t{1}".format(i, "\t".join("{0}\t{1}".format(label, prob) for label, prob in sorted(probabilities.items())))

				prediction = max_valued_key(probabilities)
				if prediction == gold_label:
					correct += 1

				confusion_matrix[gold_label][prediction] += 1

		accuracy = float(correct) / len(results)

		preamble = """\
Confusion matrix for the test data (row is the 
truth, column is the system output):
"""

		print preamble
		print matrix_to_string(confusion_matrix)
		print
		print "Test accuracy: {0:f}".format(accuracy)

#              talk.politics.guns talk.politics.misc talk.politics.mideast
# talk.politics.guns 31 39 30
# talk.politics.misc 34 42 24
# talk.politics.mideast 9 35 56

#  Test accuracy=0.43


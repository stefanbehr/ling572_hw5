# Stefan Behr
# LING 572
# Homework 5

import sys
from utils import isolate_feature

if __name__ == "__main__":
	try:
		data_path = sys.argv[1]
	except IndexError:
		exit("Please provide path to training data file.")
	try:
		out_path = sys.argv[2]
	except IndexError:
		exit("Please provide path for storing output.")

	counts = {}
	N = 0
	with open(data_path) as data_file:
		for instance in data_file:
			instance = instance.strip().split()
			if instance:	# guard against blank lines, probably not needed
				N += 1
				label = instance.pop(0)
				features = map(isolate_feature, instance)
				if label not in counts:
					counts[label] = {}
				for feature in features:
					counts[label][feature] = counts[label].get(feature, 0) + 1

	# print
	with open(out_path, "w") as out_file:
		labels = sorted(counts.keys())
		for label in labels:
			features = sorted(counts[label].keys())
			for feature in features:
				raw_count = counts[label][feature]
				expectation = float(raw_count) / N
				print >> out_file, "{0} {1} {2} {3}".format(label, feature, expectation, raw_count)
#!/opt/python-2.7/bin/python2.7

# Stefan Behr
# LING 572
# Homework 5

import sys
from math import exp
from utils import *

if __name__ == "__main__":
	help =	"""\
Three arguments expected:
	sys.argv[1] should be file containing training vectors in plaintext form
	sys.argv[2] should be target for storing results
	sys.argv[3] is an optional text model file path\
"""

	if len(sys.argv) < 3:
		print "{0} argument(s) given.".format(len(sys.argv) - 1)
		print help
		exit()
	else:
		train_path = sys.argv[1]
		out_path = sys.argv[2]


		try:
			use_model = True
			model_path = sys.argv[3]
		except IndexError:
			use_model = False

		class_indicator = "FEATURES FOR CLASS "
		default = "<default>"

		model = {}
		model_expect = {}
		class_labels = set()

		if use_model:
			# read in and store model
			with open(model_path) as model_file:
				for line in model_file:
					line = line.strip()
					if line:
						if line.startswith(class_indicator):
							current_class = line[len(class_indicator):]
							model[current_class] = {}
						else:
							feature, weight = line.split()
							weight = float(weight)
							model[current_class][feature] = weight

		# iterate over training instances
		with open(train_path) as train_file:
			N = 0
			for train_instance in train_file:
				train_instance = train_instance.strip()
				if train_instance:
					N += 1
					train_instance = train_instance.split()
					label = train_instance.pop(0)
					class_labels.add(label)
					train_instance = [isolate_feature(feat_val_pair) for feat_val_pair in train_instance]

					# calculate probabilities
					probabilities = {}
					Z = 0
					for class_label in model:
						summation = model[class_label][default]
						for feature in train_instance:
							summation += model[class_label].get(feature, 0)
						numerator = exp(summation)
						probabilities[class_label] = numerator
						Z += numerator

					probabilities = {class_label: float(numerator) / Z for class_label, numerator in probabilities.items()}

					# fill out model expectation matrix
					for class_label in class_labels:
						if class_label not in model_expect:
							model_expect[class_label] = {}
						for feature in train_instance:
							prob = probabilities[class_label] if use_model else float(1) / len(class_labels)
							model_expect[class_label][feature] = model_expect[class_label].get(feature, 0) + prob

		# print model expectations
		with open(out_path, "w") as out_file:
			labels = sorted(model_expect.keys())
			for label in labels:
				features = sorted(model_expect[label].keys())
				for feature in features:
					count = model_expect[label][feature]	# note that we never divided probability sums by N, so this is indeed the count
					expectation = float(count) / N
					print >> out_file, "{0} {1} {2} {3}".format(label, feature, expectation, count)

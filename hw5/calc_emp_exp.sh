#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 5
# Empirical expectation script

# $1 is text-format training vectors
# $2 is output target for results

python2.7 py/empex.py $1 $2
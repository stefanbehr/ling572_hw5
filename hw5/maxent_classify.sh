#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 5
# MaxEnt classifier script

# $1 is text-format test vectors
# $2 is model file in text format
# $3 is results? file
# accuracy printed to stdout, captured outside of script

python2.7 py/classify.py $1 $2 $3
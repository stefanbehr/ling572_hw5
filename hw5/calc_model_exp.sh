#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 5
# Model expectation script

# $1 is text-format training vectors
# $2 is output target for results
# $3 is optional for a model_file

python2.7 py/modex.py $1 $2 $3
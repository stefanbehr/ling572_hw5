#/bin/sh
echo "Running hw5 in $PWD"

if [ -f q1/m1.txt ];
then
	echo "Q1: q1/m1.txt exists"
else
	echo "Q1: q1/m1.txt missing or not named correctly!"
	exit 1
fi

echo "Q2: Running maxent_classify.sh"
condor_run "./maxent_classify.sh /dropbox/12-13/572/hw5/examples/test2.vectors.txt q1/m1.txt q2/res >q2/acc" 

echo "Q3: Running calc_emp_exp.sh"
condor_run "./calc_emp_exp.sh /dropbox/12-13/572/hw5/examples/train2.vectors.txt q3/emp_count"

echo "Q4-1: Running calc_model_exp.sh"
condor_run "./calc_model_exp.sh /dropbox/12-13/572/hw5/examples/train2.vectors.txt q4/model_count q1/m1.txt"

echo "Q4-2: Running calc_model_exp.sh"
condor_run "./calc_model_exp.sh /dropbox/12-13/572/hw5/examples/train2.vectors.txt q4/model_count2"

echo "Done."
